# Python Alıştırmaları

Merhabalar, bu repomuzda çeşitli Python Alıştırma Örnekleri olacaktır. 
Zaman içerisinde çalışmalarımız bu sayfada listelenecektir.

### Örnekler

1. [Loglama](https://bitbucket.org/goldpaws/python/src/79aa94faa5be47b4f47741bbaa27b6489cbf9045/Loglama_Class/?at=master) [Goldpaw - Mustafa] [Xytra - Talha]
---
Kişiselleştirilmiş loglama modülü: Kendi uygulamalarımız da kullanabileceğimiz, loglama sistemi

2. [Kangrubu Rehberi](#Deneme_Linki) [Xytra - Talha]
---
Telefon rehberi gibi düşünebilirsiniz, yakınlarınızı kayıt edebileceğiniz ve kangrublarına göre listeleyebileceğiniz program.

3. [İlk Örnek](#Deneme_Linki) [Xytra - Talha]

### Çalışmalara sizde katılabilirsiniz...

Örnek ve alıştırmalardan faydalanabilir ve sizlerde kodlarınızı göndererek, havuza katkıda bulunabilirsiniz. 

Ayrıca hoşunuza giden yada ihtiyaç duyduğunuz bir özellik veya yazılım varsa bize İssue'dan yazabilirsiniz.

### Özellikler

Kullanılan versiyon: Python 3.x

### Katkı Sağlayanlar
- Mustafa OĞUZ
- Talha SANCAR

### İyi Yazmalar