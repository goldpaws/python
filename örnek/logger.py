# Loglama Sınıfı Tasarımı
# Tarih ve Saat Yazdıran fonksiyon yazılması lazım
# Klasör ve Dosya Oluşturma Sistemi
# Dosyaya belirli formatda satır satır yazma işlemi
# Kategorileme sitemi (Warning, Error, Critacal, İnfo)

import logging

def Loglama():
    logger = logging.getLogger('Log Tutma')
    logger.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch.setFormatter(formatter)

    logger.addHandler(ch)

    logging.basicConfig(filename='./Logs/kayitlar.log', filemode='w', level=logging.DEBUG)

    logger.debug('Sistem Kayıt')
    logger.info('Bilgi')
    logger.warn('Uyarı')
    logger.error('Hata')
    logger.critical('Acıl')
