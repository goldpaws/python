from tkinter import *
from tkinter import messagebox

top = Tk()
top.geometry("400x450")
baslik=top.title("TD Kan Veri Deposu")

veri = [
    [
        # A Rh+
        {'Ad': 'Murat', 'Soyad': 'Tuncel', 'Telefon':'124563251', 'Cinsiyet': 'Erkek'},
        {'Ad': 'Mehmet', 'Soyad': 'Kalas', 'Telefon':'124563251', 'Cinsiyet': 'Erkek'},
        {'Ad': 'Ali', 'Soyad': 'Topal', 'Telefon':'124563251', 'Cinsiyet': 'Erkek'},
        {'Ad': 'Polat', 'Soyad': 'Alemdar', 'Telefon':'124563251', 'Cinsiyet': 'Erkek'},
        {'Ad': 'Kerem', 'Soyad': 'Aşık', 'Telefon':'124563251', 'Cinsiyet': 'Erkek'}
    ],
    [
        #A Rh-
        {'Ad': 'Şeyma', 'Soyad': 'Tuncel', 'Telefon':'124563251', 'Cinsiyet': 'Kadın'},
        {'Ad': 'Feyyaz', 'Soyad': 'Çakır', 'Telefon':'124563251', 'Cinsiyet': 'Erkek'},
        {'Ad': 'Seda', 'Soyad': 'Şakir', 'Telefon':'124563251', 'Cinsiyet': 'Kadın'},
        {'Ad': 'Esin', 'Soyad': 'Daima', 'Telefon':'124563251', 'Cinsiyet': 'Kadın'},
        {'Ad': 'Murat', 'Soyad': 'Çemkir', 'Telefon':'124563251', 'Cinsiyet': 'Erkek'}
    ]
]

def listele(kgrubu):
    strText = ""
    for text in veri[kgrubu]:
        strText += text['Ad'] + ' ' + text['Soyad'] + ' ' + text['Telefon'] + ' ' + text['Cinsiyet'] + ',\n'
    return strText

ArhPozitif = listele(0)
ArhNegatif = listele(1)

def Bilgiver1():
    messagebox.showinfo("A Rh (+) Sonuçları", ArhPozitif)
def Bilgiveri2():
    messagebox.showinfo("A Rh (-) Sonuçları", ArhNegatif)

def DonorFormEkle():
    donorFormEkle = Tk()
    donorFormEkle.title('Donor Ekle')
    donorFormEkle.geometry('400x600')
    label = Label(text="Ad:")
    label.pack()
    ad = Text(donorFormEkle)
    ad.pack()
    tus1 = Button(donorFormEkle, text="Ekle")
    tus1.pack(fill=BOTH)
    tus2 = Button(donorFormEkle, text="Sil")
    tus2.pack(fill=BOTH)


tus1= Button(text="A Rh (+)", command= Bilgiver1)
tus1.pack(fill=BOTH)
tus2= Button(text="A Rh (-)", command=Bilgiveri2)
tus2.pack(fill=BOTH)
tus3= Button(text="B Rh (+)")
tus3.pack(fill=BOTH)
tus4= Button(text="B Rh (-)")
tus4.pack(fill=BOTH)
tus5= Button(text="AB Rh (+)")
tus5.pack(fill=BOTH)
tus6= Button(text="AB Rh (-)")
tus6.pack(fill=BOTH)
tus7= Button(text="O Rh (+)")
tus7.pack(fill=BOTH)
tus8= Button(text="A Rh (-)")
tus8.pack(fill=BOTH)
tus9= Button(text="Donor Ekle", command=DonorFormEkle)
tus9.pack(fill=BOTH)
tus10= Button(text="Donor Sil")
tus10.pack(fill=BOTH)
mainloop()