Loglama
---
- Version : 0.3 Alpha
- Eklenen Özellikler;
    - Debug mod, sınıf parametresi olarak ayarlanabilir. `logger = Loglama(True)`
    
- Bilinen Buglar;
    - Loglama Sınıfı ayrı ayrı çağırıldığında log dosyasına ayrı ayrı erişim sağlanmaya çalışılıyor.