"""
    ----------------------------------------------------------
    - Loglama Sınıfı                                         -
    - Version : 0.3 Alpha                                    -
    - Geliştirici Ekip : Goldpaws                            -
    - Yazar : Mustafa OĞUZ                                   -
    - Açıklama : Kişiselleştirilmiş loglama sınıfı,          -
    - kendi projelerimizde kullanmak için yazılmıştır.       -
    - Lisans : GNU - README.md dosyasında yazılanları        -
    - okuyunuz, şartlar detaylı bir şekilde belirtilmiştir.  -
    ----------------------------------------------------------
"""
import os
from datetime import datetime


class Loglama:
    # Sınıf Özellikleri
    category = (
        'Normal',
        'İnfo',
        'Error',
        'Warning',
        'Critical',
    )
    # TODO: Dizin yolunu, klasör, dosya ve karakter tipi parametre olarak alınacak
    # TODO: Varsayılan değerler belirtilecek
    path = "./Kan_Grubu/"
    folder = "Logs"
    file = "Log.txt"
    charset = "utf-8"
    folderpath = path + folder
    filepath = path + folder + "/" + file
    pFile = None
    debugMode = False

    # Kurucu Metot
    def __init__(self, debug = False):
        # Debug Mode
        self.debugMode = debug

        # Dosya Kontrolü
        if os.path.exists(self.filepath):
            self.pFile = open(self.filepath, "a", encoding=self.charset)
        else:
            #Dizin Kontrolü
            if not os.path.exists(self.folderpath):
                try:
                    os.makedirs(self.folderpath)
                    self.pFile = open(self.filepath, "w", encoding=self.charset)
                    self.normal("Log dosyası oluşturuldu.")
                except:
                    print("Dizin veya dosya, işletim sistemi farkından dolayı oluşturulamadı.")
            else:
                try:
                    self.pFile = open(self.filepath, "w", encoding=self.charset)
                    self.normal("Log dosyası oluşturuldu.")
                except:
                    print("Dosya oluşturulamadı.")

    # Yıkıcı Metot
    def __del__(self):
        self.pFile.close()

    def zaman(self):
        an = datetime.strftime(datetime.now(), '%d/%m/%Y - %X')
        return an

    def logYaz(self, mesaj, categories = 0):
        if self.debugMode:
            print(self.zaman() + " - [" + self.category[0] + "]: " + mesaj + "\n")
        self.pFile.write(self.zaman() + " - [" + self.category[categories] + "]: " + mesaj + "\n")

    def normal(self, mesaj):
        self.logYaz(mesaj, 0)

    def info(self, mesaj):
        self.logYaz(mesaj, 1)

    def error(self, mesaj):
        self.logYaz(mesaj, 2)

    def warning(self, mesaj):
        self.logYaz(mesaj, 3)

    def critical(self, mesaj):
        self.logYaz(mesaj, 4)
