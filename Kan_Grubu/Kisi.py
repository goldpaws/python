"""
    ----------------------------------------------------------------
    - Kişi Sınıfı                                                  -
    - Versiyon : 0.1 Alpha                                         -
    - Geliştirici Ekip : Goldpaws                                  -
    - Yazar : Mustafa OĞUZ                                         -
    - Açıklama : Kişi bilgilerini tutan sınıf                      -
    - Lisans : GNU - README.md dosyasında yazılanları              -
    - okuyunuz, şartlar detaylı bir şekilde belirtilmiştir.        -
    ----------------------------------------------------------------
"""


class Kisi:
    # Sınıf Özellikleri
    kan_gruplari = (
        'AB Rh+',
        'AB Rh-',
        'A Rh+',
        'A Rh-',
        'B Rh+',
        'B Rh-',
        '0 Rh+',
        '0 Rh-'
    )

    cinsiyet = (
        'Erkek',
        'Kadın'""
    )

    liste = []

    def __init__(self):
        # Todo: Kurucu metodu tanımlanacak
        pass

    def __del__(self):
        # Todo: Yokedici metodu tanımlanacak
        pass

    def ekle(self):
        # Todo: Listeye Ekleme Metodu tanımlanacak
        pass

    def sil(self):
        # Todo: Listeden silme metodu tanımlanacak
        pass

    def goster(self):
        # Todo: Kişiyi göster metodu tanımlanacak.
        pass

    def listele(self):
        # Todo: Kişileri listeleme metodu tanımlanacak.
        pass

    def cinsiyet(self):
        # Todo: İnt'den Str'ye dönüştüren cinsiyet metodu tanımlanacak
        pass

    def kangrubu(self):
        # Todo: İnt'den Str'ye dönüştüren kangrubu metodu tanımlanacak
        pass

    def kontrol(self):
        # Todo: Listeyi Kontrol eden metot tanımlanacak
        pass