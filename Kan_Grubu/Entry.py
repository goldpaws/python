"""
    ----------------------------------------------------------------
    - Entry Sınıfı                                                 -
    - Versiyon : 0.1 Alpha                                         -
    - Geliştirici Ekip : Goldpaws                                  -
    - Yazar : Mustafa OĞUZ                                         -
    - Açıklama : İstenecek bilgilerin işleyen ve kontrolünü        -
    -            sağlayan sınıf                                    -
    - Lisans : GNU - README.md dosyasında yazılanları              -
    - okuyunuz, şartlar detaylı bir şekilde belirtilmiştir.        -
    ----------------------------------------------------------------
"""

class Entry():

    #Sınıf Özellikleri
    logger = None
    kisi = []

    def __init__(self, logger):
        self.logger = logger

    def TcNo(self):
        try:
            while True:
                value = input("Lütfen T.C Kimlik Numarası Giriniz:")
                if len(value) == 11:
                    return int(value)
                else:
                    print("Girdiğiniz Tc Kimlik Numarası Geçerli Değildir.")
                    self.logger.warning("Girdiğiniz Tc Kimlik Numarası Geçerli Değildir.")
        except ValueError:
            print("Tc kimlik numarası sadece rakamlardan oluşmalıdır.")
            self.logger.error("Tc kimlik numarası sadece rakamlardan oluşmalıdır.")

    def Ad(self):
        try:
            while True:
                value = input("Lütfen Ad Giriniz:")
                if len(value) >= 3 and len(value) <= 12:
                    return value
                else:
                    print("Girdiğiniz isim en az 3 ve en fazla 12 karakter olmalıdır.")
                    self.logger.warning("Girdiğiniz isim en az 3 ve en fazla 12 karakter olmalıdır.")
        except TypeError:
            print("Lütfen ad için Alfabetik karakterler kullanınız.")
            self.logger.error("Lütfen ad için Alfabetik karakterler kullanınız.")

    def Soyad(self):
        try:
            while True:
                value = input("Lütfen Soyad Giriniz:")
                if len(value) >= 3 and len(value) <= 12:
                    return value
                else:
                    print("Girdiğiniz soyad en az 3 ve en fazla 12 karakter olmalıdır.")
                    self.logger.warning("Girdiğiniz soyad en az 3 ve en fazla 12 karakter olmalıdır.")
        except TypeError:
            print("Lütfen soyad için Alfabetik karakterler kullanınız.")
            self.logger.error("Lütfen soyad için Alfabetik karakterler kullanınız.")


    # Cinsiyet Bilgisini Talep eden bir metot
    def Cinsiyet(self):
        try:
            while True:
                print("1. Bay")
                print("2. Bayan")
                value = input("Lütfen Bir Cinsiyet Tuşlayınız:")
                if len(value) >= 1 and len(value) <= 2:
                    return int(value)
                else:
                    print("Tuşladığınız Sayı Geçersiz")
                    self.logger.warning("Tuşladığınız Sayı Geçersiz")
        except ValueError:
            print("Sadece 1 Ve 2 Rakamlarını Tuşlayabilirsiniz!")
            self.logger.error("Sadece 1 Ve 2 Rakamlarını Tuşlayabilirsiniz!")

    # Telefon bilgisini talep eden metot
    def Telefon(self):
        try:
            while True:
                value = input("Lütfen Başında Sıfır Olmadan Telefon Numaransı Giriniz:")
                if len(value) >= 1 and len(value) <= 10:
                    return int(value)
                else:
                    print("Girdiğiniz Numara Geçerli Değildir!")
                    self.logger.warning("Girdiğiniz Numara Geçerli Değildir!")
        except ValueError:
            print("Girdiğiniz Numara Başında Sıfır Olmadan Rakamlardan Oluşmalıdır.")
            self.logger.error("Girdiğiniz Numara Başında Sıfır Olmadan Rakamlardan Oluşmalıdır.")

    # Yaş bilgisini talep eden metot
    def Yas(self):
        try:
            while True:
                value = int(input("Lütfen Yaşınızı Giriniz"))
                if value >= 18 and value <= 65:
                    return value
                else:
                    print("Yanlızca 18 ile 65 Yaş Arası Kan Verilebilir")
                    self.logger.warning("Yanlızca 18 ile 65 Yaş Arası Kan Verilebilir")
        except ValueError:
            print("Yanlızca 18 ile 65 Yaş Arası Kan Verilebilir")
            self.logger.warning("Yanlızca 18 ile 65 Yaş Arası Kan Verilebilir")

    # Kan Grubu bilgisini talep eden metot
    def KanGrubu(self):
        try:
            while True:
                print("1.A Rh+", "2.A Rh-", "3.B Rh+", "4.B Rh-", "5.AB Rh+", "6.AB Rh-", "7.0 Rh+", "8.0 Rh-")
                liste = [""], ["1.A Rh+"], ["2.A Rh-"], ["3.B Rh+"], ["4.B Rh-"], ["5.AB Rh+"], ["6.AB Rh-"], ["7.0 Rh+"], [
                    "8.0 Rh-"],
                value = input("Lütfen Kan Gurubunu Tuşlayınız!")
                if len(value) >= 1 and len(value) <= 8:
                    return int(value)
                else:
                    print("Hatalı Tuşlama!")
                    self.logger.warning("Hatalı Tuşlama")

        except ValueError:
            print("Lütfen 1 ile 8 Arasında Ki Rakamları Tuşlayınız")
            self.logger.warning("Lütfen 1 ile 8 Arasında Ki Rakamları Tuşlayınız")

    # TODO: Bütün bu işlemleri içeren metot tanımlacak
    """
        Oluşturduğumuz bu metotları tek metotda toplamak ve Kullanıcıdan
        alınan bilgileri geriye bir liste olarak döndürmek.
    """
    # Kullanıcıdan veri talep eden metot
    def VeriAl(self):
        pass