# Kangrubu Sistemi

from Loglama_Class import Loglama
from Kan_Grubu import Dosya
from Kan_Grubu import Entry

print("-----------------------------------")
print("-- Kan_Grubu Sistemi             --")
print("-- Version : 0.3 Alpha           --")
print("-----------------------------------")

logger = Loglama.Loglama()
dosya = Dosya.Dosya(logger)
entry = Entry.Entry(logger)

veri = 5

if veri:
    print("Şuan sistemimizde {} adet donor kayıtlıdır.".format(veri))
else:
    print("Sistemde kayıtlı donor bulunamadı.")

while True:
    print("---------------------------")
    print("1. Donor Ekle")
    print("2. Donor Sil")
    print("3. Donor Listele")
    print("4. Kangrubuna göre listele")
    print("5. Çıkış \n")
    u_input = int(input("Yapmak istediğiniz işlemi seçiniz: "))

    # Donor Ekle(str ve int bugu düzeltilecek)
    if u_input == 1:
        print("---------------------------------------------")
        TcNo = entry.TcNo()
        Ad = entry.Ad()
        Soyad = entry.Soyad()
        Cinsiyet = entry.Cinsiyet()
        Yas = entry.Yas()
        Telefon = entry.Telefon()
        KanGrubu = entry.KanGrubu()
        print("---------------------------------------------")
        #TODO : Listeye Eklenecek
        #TODO : Kayıt ve Loading işlemleri yazılacak
        print(Ad, Soyad, Cinsiyet, Yas, Telefon, KanGrubu, "\n")
        strFormat = "{} {} {} {} {} {}\n".format(Ad, Soyad, Cinsiyet, Yas, Telefon, KanGrubu)
        kisi = strFormat
        dosya.Ekle(kisi)
        print("Donor kayıt edildi.")
        logger.normal(Ad + " " + Soyad + " adlı donur kayıt edildi.")
    # Donor Sil
    elif u_input == 2:
        #TODO : Listeden Sil ve Veritabanından sil
        print("Donor Silindi. \n")
        logger.info("Donor Silindi.")
    # Donor Listele
    elif u_input == 3:
        #TODO : Loading işlemi ile hafızada ki veriyi listele
        print("---------------------------------------")
        print("-- Donor Listesi                     --")
        print("1. Ahmet UÇARSU - ERKEK - 25 - A Rh+ ")
        print("\n")
        logger.warning("Donor Listelendi.")
    # Donor Kangrubuna Göre Listele
    elif u_input == 4:
        #TODO: Kangrubuna göre listele
        print("---------------------------------------")
        print("Donor Listelendi.")
        logger.error("Donor Listelendi.")
    # Program Sonlandırıldı.
    else:
        print("Program sonlandırıldı.")
        logger.normal("Program Sonlandırıldı.")
        break