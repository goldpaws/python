"""
    ----------------------------------------------------------------
    - Dosya Sınıfı                                                 -
    - Versiyon : 0.2 Alpha                                         -
    - Geliştirici Ekip : Goldpaws                                  -
    - Yazar : Mustafa OĞUZ                                         -
    - Açıklama : Verileri dosyalayan sınıf                         -
    - Lisans : GNU - README.md dosyasında yazılanları              -
    - okuyunuz, şartlar detaylı bir şekilde belirtilmiştir.        -
    ----------------------------------------------------------------
"""
import os

class Dosya:
    # Sınıfın Özellikleri
    path = "./Kan_Grubu/"
    folder = ""
    file = "Kisiler.txt"
    charset = "utf-8"
    folderpath = path + folder
    filepath = path + folder + "/" + file
    pFile = None

    logger = None

    # Kurucu Metot
    def __init__(self, logger):

        self.logger = logger

        # TODO: Gereksiz görünüyor, Kontrol edildikten sonra silinebilir.
        # Dosya Kontrolü
        if os.path.exists(self.filepath):
            self.pFile = open(self.filepath, "a", encoding=self.charset)
            self.pFile.close()
        else:
            # Dizin Kontrolü
            if not os.path.exists(self.folderpath):
                try:
                    os.makedirs(self.folderpath)
                    self.pFile = open(self.filepath, "w", encoding=self.charset)
                    self.logger.normal(self.file + " adlı dosya oluşturuldu")
                    self.pFile.close()
                except:
                    print("Dizin veya dosya, işletim sistemi farkından dolayı oluşturulamadı.")
            else:
                try:
                    self.pFile = open(self.filepath, "w", encoding=self.charset)
                    self.logger.normal(self.file + " adlı dosya oluşturuldu")
                    self.pFile.close()
                except:
                    print("Dosya oluşturulamadı.")

    #Yıkıcı Metot
    def __del__(self):
        if not type(None):
            self.pFile.close()

    # Veri Ekleme Metodu
    def Ekle(self, params = []):
        self.pFile = open(self.filepath, "a", encoding=self.charset)
        self.pFile.writelines(params)
        self.logger.normal("Veri Eklendi.")
        self.pFile.close()

    # TODO: Veri Okuma Metodu Tanımlanacak
    """
        Veriler dosya'ya nasıl ekleniyorsa, veriler okunacak.
        Veriler satır satır ayrıştırılıp, düzenli bir şekilde listeye eklenecek
        Bu liste geriye liste olarak döndürülecek.
        Not: Satır sonlarındaki '\n' satır atlama işaretleri temizlenecek.
    """
    # Veri Okuma Metodu
    def Oku(self):
        pass

    # TODO: Dosya İçeriğini temizleme metodu tanımlacak
    """
        Dosya'nın varlığını silmeden, içeriğini temizleyen metot tanımlanacak.
    """
    def Temizle(self):
        pass

    # TODO: Dosya'yı silen Metot Tanımlacak
    """
        Dosyayı silme işleminden önce kullanıcının işleminden emin olup, olmadığı
        sorulacak.
        Onay alındıktan sonra, dosyanın açık bağlantısının olup olmadığı
        kontrol edildikten sonra dosyanın silinmesi
    """
    # Dosyayı silen metot
    def Sil(self):
        pass